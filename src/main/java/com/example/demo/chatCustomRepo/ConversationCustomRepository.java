

package com.example.demo.chatCustomRepo;

import java.util.List;

import com.example.demo.pojo.ChatMessage;
import com.example.demo.pojo.Conversation;



// TODO: Auto-generated Javadoc
/**
 * The Interface ConversationCustomRepository.
 */
public interface ConversationCustomRepository {
	
	
	/**
	 * Find all chat by sender id.
	 *
	 * @param id the id
	 * @return the list
	 */
	public List<Conversation> findAllChatBySenderId(String id);
	
	/**
	 * Find all chat by chat id.
	 *
	 * @param id the id
	 * @param startIndex the start index
	 * @param limit the limit
	 * @return the list
	 */
	public List<ChatMessage>  findAllChatByChatId(String id, int startIndex ,int limit);

	/**
	 * Find chat by chat id.
	 *
	 * @param id the id
	 * @return the conversation
	 */
	public Conversation findChatByChatId(String id);
	
	/**
	 * Update status by message id.
	 *
	 * @param _id the id
	 * @param chatId the chat id
	 */
	
	
}

package com.example.demo.chatCustomRepoImpl;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.AggregationOptions;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.TypedAggregation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.example.demo.chatCustomRepo.ConversationCustomRepository;
import com.example.demo.pojo.ChatMessage;
import com.example.demo.pojo.Conversation;



// TODO: Auto-generated Javadoc
/**
 * The Class ConversationRepositoryImpl.
 */
@Repository
public class ConversationRepositoryImpl implements ConversationCustomRepository {

	/** The operations. */
	@Autowired
	public MongoOperations operations;

	/* (non-Javadoc)
	 * @see com.mavenworkforce.mavenmate.customRepository.ConversationCustomRepository#findAllChatBySenderId(java.lang.String)
	 */
	@Override
	public List<Conversation> findAllChatBySenderId(String id) {
		Query query = new Query();
		query.addCriteria(Criteria.where("userIds").all(id));
		query.fields().exclude("chat");
		List<Conversation> chat = operations.find(query, Conversation.class);
		return chat;
	}
	
	/* (non-Javadoc)
	 * @see com.mavenworkforce.mavenmate.customRepository.ConversationCustomRepository#findChatByChatId(java.lang.String)
	 */
	@Override
	public Conversation findChatByChatId(String id) {
		Query query = new Query();
		query.addCriteria(Criteria.where("conId").is(id));
		
		return operations.findOne(query, Conversation.class);
	}


	/* (non-Javadoc)
	 * @see com.mavenworkforce.mavenmate.customRepository.ConversationCustomRepository#findAllChatByChatId(java.lang.String, int, int)
	 */
	@Override
	public List<ChatMessage> findAllChatByChatId(String id, int startIndex, int limit) {
		List<AggregationOperation> list = new ArrayList<AggregationOperation>();
		list.add(Aggregation.match(Criteria.where("conId").is(id)));
		list.add(Aggregation.unwind("chat"));
		long skip = (startIndex > 0) ? startIndex * limit : 0;
		list.add(Aggregation.limit(skip + limit));
		list.add(Aggregation.skip(skip));
		list.add(Aggregation.group("conId").push("chat").as("chat"));

		AggregationOptions.Builder builder = new AggregationOptions.Builder();
		builder.cursor(new Document());
		TypedAggregation<Conversation> agg = Aggregation.newAggregation(Conversation.class, list)
				.withOptions(builder.build());
		AggregationResults<Conversation> aggregate = operations.aggregate(agg, Conversation.class, Conversation.class);
		Document rawResults = aggregate.getRawResults();
		LogManager.getLogger().info(rawResults.toJson());
		List<Conversation> mappedResults = aggregate.getMappedResults();
		Conversation chat = mappedResults.get(0);
		// Query query = new Query();
		// query.addCriteria(C)

		if (chat == null)
			return null;
		return chat.getChat();

	}

	/* (non-Javadoc)
	 * @see com.mavenworkforce.mavenmate.customRepository.ConversationCustomRepository#updateStatusByMessageId(java.lang.String, java.lang.String)
	 */


}

package com.example.demo.chatRepo;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.chatCustomRepo.ConversationCustomRepository;
import com.example.demo.pojo.Conversation;

import java.lang.String;

/**
 * The Interface ConversationRepository.
 *
 * @author vijay
 */
@Repository
public interface ConversationRepository extends MongoRepository<Conversation, String>, ConversationCustomRepository {



}

package com.example.demo.chatRepo;



import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.pojo.User;

import java.lang.String;

// TODO: Auto-generated Javadoc
/**
 * The Interface UserRepository.
 *
 * @author Amit Kumar
 *         <p>
 *         User Repository which is connnected to Mongodb
 */
@Repository
public interface UserRepository extends MongoRepository<User, String> {

	/**
	 * Find by email.
	 *
	 * @param email the email
	 * @return the list
	 */
	// @Query(fields = "{'pwd':1}")

}

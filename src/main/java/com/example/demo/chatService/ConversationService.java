package com.example.demo.chatService;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.chatRepo.ConversationRepository;
import com.example.demo.pojo.ChatMessage;
import com.example.demo.pojo.Conversation;


// TODO: Auto-generated Javadoc
/**
 * The Class ConversationService.
 */
@Service
public class ConversationService {

	
	/** The chat message repository. */
	@Autowired
	private ConversationRepository chatMessageRepository;

	/**
	 * Save.
	 *
	 * @param chat the chat
	 * @return the conversation
	 */
	public Conversation save(Conversation chat) {
		return  chatMessageRepository.save(chat);
		
	}

	/**
	 * Find all chat by sender id.
	 *
	 * @param id the id
	 * @return the list
	 */
	public List<Conversation> findAllChatBySenderId(String id) {
		return chatMessageRepository.findAllChatBySenderId(id);
	}

	/**
	 * Find chat by chat id.
	 *
	 * @param id the id
	 * @return the conversation
	 */
	public Conversation findChatByChatId(String id) {
		return chatMessageRepository.findChatByChatId(id);
		
	}
	
	/**
	 * Find all chat by chat id.
	 *
	 * @param id the id
	 * @param startIndex the start index
	 * @param limit the limit
	 * @return the list
	 */
	public List<ChatMessage> findAllChatByChatId(String id, int startIndex, int limit) {
		return chatMessageRepository.findAllChatByChatId(id, startIndex, limit);
	}
	 
	
	/**
	 * @param repository
	 */
}
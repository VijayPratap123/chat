package com.example.demo.chatService;

import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.example.demo.chatRepo.UserRepository;
import com.example.demo.pojo.User;


// TODO: Auto-generated Javadoc
/**
 * The Class UserService.
 *
 * @author Amit Kumar
 *         <p>
 *         User CRUD operations is Operated by UserService
 */
@Service
public class UserService {

	/** User Repository. */
	private final UserRepository mRepository;

	/**
	 * Instantiates a new user service.
	 *
	 * @param repository the repository
	 */
	@Autowired
	public UserService(UserRepository repository) {
		this.mRepository = repository;
	}

	/**
	 * create new User.
	 *
	 * @param todo the todo
	 * @return the completable future
	 * 
	 * 
	 */
	public CompletableFuture<User> findById(String id) {
		Optional<User> optional = mRepository.findById(id);
		if (optional.isPresent())
			return CompletableFuture.completedFuture(optional.get());
		else
			return CompletableFuture.completedFuture(null);
	}
	public User save(User user) {
		mRepository.save(user);
		return user;
	}

}
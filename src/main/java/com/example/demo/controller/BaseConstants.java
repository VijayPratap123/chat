package com.example.demo.controller;

public interface BaseConstants {

	String	SLASH					= "/";
	String	LOCAL_HOST				= "http://localhost:8080";
	String	REMOTE_HOST				= "https://api.maven.com";
	String	API_VERSION				= "v2";
	String	API_BASE_URL			= SLASH + API_VERSION;
	String	ENDPOINT_NON_LOGIN_USER	= API_BASE_URL + "/view";

	String	ENDPOINT_PAGE	= "/page";
	String	ENDPOINT_SIZE	= "/size";

	String	HEADER_AUTH_TOKEN	= "X-Maven-REST-AUTH-TOKEN";
	String	ORIGIN				= "Origin";
	String	METHODS				= "GET, PUT, POST, DELETE, HEAD";

	String HEADERS = "*";

	String	HEADER_ACCESS_CONTROL_ALLOW_ORIGIN		= "Access-Control-Allow-Origin";
	String	HEADER_ACCESS_CONTROL_ALLOW_HEADERS		= "Access-Control-Allow-Headers";
	String	HEADER_ACCESS_CONTROL_ALLOW_METHODS		= "Access-Control-Allow-Methods";
	String	HEADER_ACCESS_CONTROL_ALLOW_CREDENTIALS	= "Access-Control-Allow-Credentials";

	/**
	 * The unique ID for a row.
	 * <P>
	 * Type: INTEGER (long)
	 * </P>
	 */
	public static final String _ID = "_id";

	/**
	 * The count of rows in a directory.
	 * <P>
	 * Type: INTEGER
	 * </P>
	 */
	public static final String _COUNT = "_count";

	/*
	 * The current transaction status. May be null if no current transaction for
	 * this resource <P>Type: STRING</P>
	 */
	public static final String _STATUS = "_status";

	/*
	 * The transaction result code, typically an http status. <P>Type: INTEGER </P>
	 */
	public static final String _RESULT = "_result";

	// no of page
	String PAGE = "page";

	// item size on page
	String SIZE = "size";

}

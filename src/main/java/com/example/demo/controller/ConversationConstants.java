package com.example.demo.controller;

public interface ConversationConstants extends BaseConstants {
	String PARAM_USER_ID = "_id";
	String REQUEST_TYPE = "requestType";
	String BLOCK_USER_ID = "userId";
	String BLOCK = "messageing BLOCK";
	String UN_BLOCK = "messageing UN_BLOCK";
	String MESSAGE_ID = "_id";
	String STATUS = "status";
	String CHAT_ID = "conId";
	String MESSAGE_STATUS = "read";
	String TYPE_NOTIFICATION = "notification";
	String CHAT  = "chat";
	String USER_IdS= "userIds";
	String HISTORY = "/history";

	String ENDPOINT_USER_CHAT = "/chat";
	String ENDPOINT_MESSAGEID = "/messageId";
	String ENDPOINT_BLOCK = "/block";
	String ENDPOINT_USER = "/user";
	String ENDPOINT_USER_WITH_API_BASE_URL = API_BASE_URL + ENDPOINT_USER;
	String ENDPOINT_CHAT = ENDPOINT_USER_WITH_API_BASE_URL + "/chat";
	String ENDPOINT_USER_CHAT_HISTORY = ENDPOINT_USER_WITH_API_BASE_URL + "/{" + PARAM_USER_ID + "}";
	String ENDPOINT_TWO_USER_CHAT_HISTORY = ENDPOINT_USER_WITH_API_BASE_URL + "/{" + PARAM_USER_ID + "}" + "/{" + PAGE
			+ "}" + "/{" + SIZE + "}" + "/history";
	String ENDPOINT_TWO_USER_BLOCK = ENDPOINT_USER_WITH_API_BASE_URL + "/chatBlock";
	String ENDPOINT_GLOBAL_USER_BLOCK = ENDPOINT_USER_WITH_API_BASE_URL + "/{" + PARAM_USER_ID + "}/block" + "/{"
			+ BLOCK_USER_ID + "}";
	String ENDPOINT_CONVERSATION_STATUS = ENDPOINT_USER_WITH_API_BASE_URL + "/messageId" + "/{" + MESSAGE_ID + "}"
			+ "/chat" + "/{" + CHAT_ID + "}";

}

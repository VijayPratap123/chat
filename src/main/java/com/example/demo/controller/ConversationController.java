package com.example.demo.controller;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpServletResponse;

import org.bson.types.ObjectId;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.chatService.ConversationService;
import com.example.demo.chatService.UserService;
import com.example.demo.pojo.BlockRequest;
import com.example.demo.pojo.ChatMessage;
import com.example.demo.pojo.ChatRequest;
import com.example.demo.pojo.Conversation;
import com.example.demo.pojo.User;
import com.example.demo.pojo.UserBlockRequest;



// TODO: Auto-generated Javadoc
/**
 * The Class ConversationController.
 *
 * @author vijay
 */
@RestController
public class ConversationController extends BaseController {

	/** The logger. */
	private static org.apache.logging.log4j.Logger logger = org.apache.logging.log4j.LogManager
			.getLogger(ConversationController.class);

	/** The chat service. */
	private ConversationService chatService;

	/** The service. */
	private UserService service;

	/** The session service. */
	

	/** The pushservice. */
	//private FCMNotificationServic pushservice;

	/**
	 * Instantiates a new conversation controller.
	 *
	 * @param chatService
	 *            the chat service
	 * @param service
	 *            the service
	 * @param sessionService
	 *            the session service
	 * @param pushservice
	 *            the pushservice
	 */
	@Autowired
	public ConversationController(ConversationService chatService, UserService service 
		) {
		this.chatService = chatService;
	
		this.service = service;
		

		// TODO Auto-generated constructor stub
	}
	// This RestController for Start Conversation Between Two Users...

	/**
	 * Chat.
	 *
	 * @param authToken
	 *            the auth token
	 * @param chatRequest
	 *            the chat request
	 * @param response
	 *            the response
	 * @return the response entity
	 * @throws JSONException 
	 */
//	@ApiOperation(value = "Stat conversation ", response = Conversation.class)
	@RequestMapping(value = ConversationConstants.ENDPOINT_CHAT, method = RequestMethod.POST)
	ResponseEntity<?> chat( @RequestBody ChatRequest chatRequest,
			HttpServletResponse response)  {
		logger.info("start chat...");
		// check request from valid user or not
		
		User user = getUser(chatRequest.getChatMessage().getSenId());

		if (user != null && user.getStatus() != null) {

			if (user.getStatus().equals(ConversationConstants.BLOCK)) {
				return ResponseEntity.ok("User is block.");
			}
			return newresponseNotFound(logger, "User does not exit");
		}
		Conversation conversation = new Conversation();
		ChatMessage chatMessage = new ChatMessage();
		// check chatId null or not
		if (chatRequest.getId() == null) {
			return newresponseNotFound(logger, "Chat Id not null");
		}
		Conversation conversations = chatService.findChatByChatId(chatRequest.getId());
		//JSONObject data = new JSONObject();
		//data.put("text", chatRequest.getChatMessage().getText());
		if (conversations != null) {
			if (conversations.getBlockId() != null
					&& conversations.getBlockId().equals(chatRequest.getChatMessage().getSenId())) {
				return newresponseNotAcceptable(logger, ConversationConstants.BLOCK);

			}
			logger.info("verifying chat...");
			chatMessage = chatRequest.getChatMessage();
			chatMessage.setSentTime(System.currentTimeMillis());
			chatMessage.setId(new ObjectId().toString());
			if (conversations.getUserIds() == null) {
				conversations.setUserIds(chatRequest.getUserIds());
			}

			if (conversations.getChat() != null) {
				conversations.getChat().add(chatMessage);
			} else {
				List<ChatMessage> chat = new ArrayList<>();
				chat.add(chatMessage);
				conversations.setChat(chat);

			}
			// save conversations if user have already chatId
			conversations.setLastmessage(chatRequest.getChatMessage().getText());
			conversation = chatService.save(conversations);
			// push notification
			//pushservice.sendByUserId("message", ConversationConstants.TYPE_NOTIFICATION, chatMessage.getRecId(), data);
			return ResponseEntity.ok(conversation);
		}

		// new conversation start
		if (chatRequest.getChatMessage().getSenId() != null && chatRequest.getChatMessage().getRecId() != null) {
			logger.info("start new  chat...");
			conversation.setConId(chatRequest.getId());
			conversation.setUserIds(chatRequest.getUserIds());
			List<ChatMessage> chat = new ArrayList<>();
			chatMessage = chatRequest.getChatMessage();
			chatMessage.setSentTime(System.currentTimeMillis());
			chatMessage.setId(new ObjectId().toString());
			chat.add(chatMessage);
			conversation.setChat(chat);
			// save new conversation
			conversation.setLastmessage(chatRequest.getChatMessage().getText());
			conversation = chatService.save(conversation);
			//pushservice.sendByUserId("message", ConversationConstants.TYPE_NOTIFICATION, chatMessage.getRecId(), data);
			return ResponseEntity.ok(conversation);
		}
		return newresponseNotFound(logger, "User does not exit");

	}

	/**
	 * User all chat list.
	 *
	 * @param authToken
	 *            the auth token
	 * @param id
	 *            the id
	 * @return the response entity
	 */
	// This RestController for Find All Chat single user...
	
	@RequestMapping(value = ConversationConstants.ENDPOINT_USER_CHAT_HISTORY, method = RequestMethod.GET)
	ResponseEntity<?> userAllChatList(
			@PathVariable("userId") String id) {
		logger.info("verifying chat...");
		
		User user = getUser(id);
		if (user != null)

		{

			// find user all conversations
			List<Conversation> list = chatService.findAllChatBySenderId(id);
			if (list != null) {

				logger.info(list.size() + " Chat found by sender Id.");
				return ResponseEntity.ok(list);
			}
			return newresponseNotFound(logger, "no Message");

		}
		return newresponseNotFound(logger, "User does not exit");

	}

	// This RestController for Find Conversation Between Two Users...

	/**
	 * User chat history.
	 *
	 * @param authToken
	 *            the auth token
	 * @param chatId
	 *            the chat id
	 * @param page
	 *            the page
	 * @param size
	 *            the size
	 * @return the response entity
	 */
//	@ApiOperation(value = "Two user  conversation history ", response = ChatMessage.class, responseContainer = "List")
	@RequestMapping(value = ConversationConstants.ENDPOINT_TWO_USER_CHAT_HISTORY, method = RequestMethod.GET)
	ResponseEntity<?> userChatHistory(
			@PathVariable("userId") String chatId, @PathVariable(BaseConstants.PAGE) int page,
			@PathVariable(BaseConstants.SIZE) int size) {
		logger.info("verifying chat history...");
		if (chatId != null) {

			// PageRequest pageRequest = new PageRequest(page, size);
			List<ChatMessage> chat = chatService.findAllChatByChatId(chatId, page, size);
			// Conversation chat =chatService.findAllChatByChatId(chatId, page, size);
			// Conversation chat = chatService.findAllChatByChatId(chatId);

			logger.info(chat + " users  found.");

			return ResponseEntity.ok(chat);
		}
		return newresponseNotFound(logger, "Chat does not exit");

	}

	/**
	 * Block user.
	 *
	 * @param authToken
	 *            the auth token
	 * @param blockRequest
	 *            the block request
	 * @return the response entity
	 */
	// This RestController for Block user to user .user can block another user..
	// @ApiOperation(value = "Block user ", response = String.class)
	@RequestMapping(value = ConversationConstants.ENDPOINT_TWO_USER_BLOCK, method = RequestMethod.POST)
	ResponseEntity<?> blockUser(
			@RequestBody BlockRequest blockRequest) {
		logger.info("block chat ...");
		// find two user conversation history for block message
		Conversation conversations = chatService.findChatByChatId(blockRequest.getId());
		Conversation conversation = new Conversation();
		// request Type for block or un block 1 for block any for unblock
		if (blockRequest.getRequestType() == 1) {

			if (conversations != null) {
				logger.info("verifying chat...");
				conversations.setBlockId(blockRequest.getBlockId());
				conversations.setBlockStatus(ConversationConstants.BLOCK);
				// save conversation after block
				conversation = chatService.save(conversations);
				return ResponseEntity.ok(ConversationConstants.BLOCK);
			}
			logger.info("block user chat...");
			conversation.setConId(blockRequest.getId());
			conversation.setBlockId(blockRequest.getBlockId());
			conversation.setBlockStatus(ConversationConstants.BLOCK);
			// save conversations after block
			conversation = chatService.save(conversation);
			return ResponseEntity.ok(ConversationConstants.BLOCK);
		} else if (conversations != null) {

			logger.info("Un Block user...");
			conversations.setBlockId(null);
			conversations.setBlockStatus(ConversationConstants.UN_BLOCK);
			// save conversation after un block..
			conversation = chatService.save(conversations);

			return ResponseEntity.ok(ConversationConstants.UN_BLOCK);
		}

		return newresponseNotFound(logger, "Chat does not exit");

	}

	// This RestController for Block user Globali .user can not sent message any
	/**
	 * User globel block.
	 *
	 * @param authToken
	 *            the auth token
	 * @param _id
	 *            the id
	 * @param userId
	 *            the user id
	 * @param userBlock
	 *            the user block
	 * @param response
	 *            the response
	 * @return the response entity
	 */
	// users...
	// @ApiOperation(value = "Globel user block ", response = String.class)
	@RequestMapping(value = ConversationConstants.ENDPOINT_GLOBAL_USER_BLOCK, method = RequestMethod.POST)
	ResponseEntity<?> userGlobelBlock(
			@PathVariable("userId") String _id, @PathVariable(ConversationConstants.BLOCK_USER_ID) String userId,
			@RequestBody UserBlockRequest userBlock, HttpServletResponse response) {
	
		// userBlock.setUserId(userId);
		// find user by user id ..
		User user = getUser(userId);
		if (user == null) {
			logger.warn("No user in database");
			return newresponseNotFound(logger, "User not found.");

		}

		if (user.getReports() != null) {
			if (user.getReports().size() == 3) {
				user.setStatus(ConversationConstants.BLOCK);
				// save user after globali block
				service.save(user);
				return ResponseEntity.ok(ConversationConstants.BLOCK);
			}
			user.getReports().add(userBlock);
		} else {
			List<UserBlockRequest> report = new ArrayList<>();
			report.add(userBlock);
			user.setReports(report);

		}
		service.save(user);
		return ResponseEntity.ok(ConversationConstants.BLOCK);
	}


	public User getUser(String id) {
		User user = null;
		try {
			user = service.findById(id).get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return user;
	}

}

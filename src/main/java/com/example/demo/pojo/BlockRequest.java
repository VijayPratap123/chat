package com.example.demo.pojo;



// TODO: Auto-generated Javadoc
/**
 * The Class BlockRequest.
 */

public class BlockRequest {
	
	
	/** The id. */
	
	private String id;
	
	/** The block id. */
	
	private String blockId;
	
	/** The request type. */
	// requestType for block or unBlock
	
	private int requestType;
	

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Gets the request type.
	 *
	 * @return the request type
	 */
	public int getRequestType() {
		return requestType;
	}

	/**
	 * Sets the request type.
	 *
	 * @param requestType the new request type
	 */
	public void setRequestType(int requestType) {
		this.requestType = requestType;
	}

	/**
	 * Gets the block id.
	 *
	 * @return the block id
	 */
	public String getBlockId() {
		return blockId;
	}

	/**
	 * Sets the block id.
	 *
	 * @param blockId the new block id
	 */
	public void setBlockId(String blockId) {
		this.blockId = blockId;
	}

}

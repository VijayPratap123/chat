package com.example.demo.pojo;



// TODO: Auto-generated Javadoc
/**
 * The Class ChatMessage.
 *
 * @author vijay
 */

public class ChatMessage {

	/** The id. */
	
	
	private String id;
	
	/** The sen id. */

	private String senId;
	
	/** The rec id. */
	
	private String recId;
	
	/** The text. */

	private String text;
	
	/** The status. */

	private String status;
	
	/** The rec time. */
	
	private long recTime;
	
	/** The sent time. */

	private long sentTime;
	

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Gets the sen id.
	 *
	 * @return the sen id
	 */
	public String getSenId() {
		return senId;
	}

	/**
	 * Sets the sen id.
	 *
	 * @param senId the new sen id
	 */
	public void setSenId(String senId) {
		this.senId = senId;
	}

	/**
	 * Gets the rec id.
	 *
	 * @return the rec id
	 */
	public String getRecId() {
		return recId;
	}

	/**
	 * Sets the rec id.
	 *
	 * @param recId the new rec id
	 */
	public void setRecId(String recId) {
		this.recId = recId;
	}

	/**
	 * Gets the text.
	 *
	 * @return the text
	 */
	public String getText() {
		return text;
	}

	/**
	 * Sets the text.
	 *
	 * @param text the new text
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Gets the sent time.
	 *
	 * @return the sent time
	 */
	public long getSentTime() {
		return sentTime;
	}

	/**
	 * Sets the sent time.
	 *
	 * @param sentTime the new sent time
	 */
	public void setSentTime(long sentTime) {
		this.sentTime = sentTime;
	}

	/**
	 * Gets the rec time.
	 *
	 * @return the rec time
	 */
	public long getRecTime() {
		return recTime;
	}

	/**
	 * Sets the rec time.
	 *
	 * @param recTime the new rec time
	 */
	public void setRecTime(long recTime) {
		this.recTime = recTime;
	}

}

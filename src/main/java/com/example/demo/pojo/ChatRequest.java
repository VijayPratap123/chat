package com.example.demo.pojo;


// TODO: Auto-generated Javadoc

/**
 * The Class ChatRequest.
 */

public class ChatRequest {
	
	/** The id. */
	
	private String id;
	
	/** The user ids. */

	private String[] userIds;
	
	/** The chat message. */
	
	private ChatMessage chatMessage;

	/**
	 * Gets the user ids.
	 *
	 * @return the user ids
	 */
	public String[] getUserIds() {
		return userIds;
	}

	/**
	 * Sets the user ids.
	 *
	 * @param userIds the new user ids
	 */
	public void setUserIds(String[] userIds) {
		this.userIds = userIds;
	}

	/**
	 * Gets the chat message.
	 *
	 * @return the chat message
	 */
	public ChatMessage getChatMessage() {
		return chatMessage;
	}

	/**
	 * Sets the chat message.
	 *
	 * @param chatMessage the new chat message
	 */
	public void setChatMessage(ChatMessage chatMessage) {
		this.chatMessage = chatMessage;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(String id) {
		this.id = id;
	}
	
	

}

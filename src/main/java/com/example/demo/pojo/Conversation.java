package com.example.demo.pojo;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;



// TODO: Auto-generated Javadoc
/**
 * The Class Conversation.
 *
 * @author vijay
 */
@Document

public class Conversation {

	/** The id. */
	// chatId
    @Id

    private String id;
    
    /** The con id. */
  
	private String conId;
	
	/** The user ids. */
	// two userId
    
	private String[] userIds;
    
    /** The block id. */
   
	private String blockId;
    
    /** The block status. */
  
	private String blockStatus;
    
    /** The lastmessage. */
 
	private String lastmessage;
    
    /** The chat. */
    
	private List<ChatMessage> chat;
	
	

	/**
	 * Gets the user ids.
	 *
	 * @return the user ids
	 */
	public String[] getUserIds() {
		return userIds;
	}

	/**
	 * Sets the user ids.
	 *
	 * @param userIds the new user ids
	 */
	public void setUserIds(String[] userIds) {
		this.userIds = userIds;
	}

	/**
	 * Gets the chat.
	 *
	 * @return the chat
	 */
	public List<ChatMessage> getChat() {
		return chat;
	}

	/**
	 * Sets the chat.
	 *
	 * @param chat the new chat
	 */
	public void setChat(List<ChatMessage> chat) {
		this.chat = chat;
	}

	/**
	 * Gets the block id.
	 *
	 * @return the block id
	 */
	public String getBlockId() {
		return blockId;
	}

	/**
	 * Sets the block id.
	 *
	 * @param blockId the new block id
	 */
	public void setBlockId(String blockId) {
		this.blockId = blockId;
	}

	/**
	 * Gets the block status.
	 *
	 * @return the block status
	 */
	public String getBlockStatus() {
		return blockStatus;
	}

	/**
	 * Sets the block status.
	 *
	 * @param blockStatus the new block status
	 */
	public void setBlockStatus(String blockStatus) {
		this.blockStatus = blockStatus;
	}

	/**
	 * Gets the lastmessage.
	 *
	 * @return the lastmessage
	 */
	public String getLastmessage() {
		return lastmessage;
	}

	/**
	 * Sets the lastmessage.
	 *
	 * @param lastmessage the new lastmessage
	 */
	public void setLastmessage(String lastmessage) {
		this.lastmessage = lastmessage;
	}

	/**
	 * Gets the con id.
	 *
	 * @return the con id
	 */
	public String getConId() {
		return conId;
	}

	/**
	 * Sets the con id.
	 *
	 * @param conId the new con id
	 */
	public void setConId(String conId) {
		this.conId = conId;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	

}

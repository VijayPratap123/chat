package com.example.demo.pojo;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

// TODO: Auto-generated Javadoc
/**
 * The Class User.
 */
@Document

public class User {

	/** The otp. */
	@Id
	private String id;
	private List<UserBlockRequest> reports;
	
	private String status;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<UserBlockRequest> getReports() {
		return reports;
	}

	public void setReports(List<UserBlockRequest> reports) {
		this.reports = reports;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
